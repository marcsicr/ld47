﻿
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RoundaboutGenerator))]
public class RoundaboutGeneratorEditor : Editor
{
    private RoundaboutGenerator generator;

    public override void OnInspectorGUI() {

        generator = target as RoundaboutGenerator;

        EditorGUILayout.BeginHorizontal();
        generator.lanePrefab = EditorGUILayout.ObjectField("Lane Prefab", generator.lanePrefab, typeof(GameObject), false) as GameObject;
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Lanes");
        int newLanes = EditorGUILayout.IntSlider(generator.numLanes, 1, 5, null);
        EditorGUILayout.EndHorizontal();

        if (newLanes != generator.numLanes) {
            generator.numLanes = newLanes;
            regenerateLanes();
        }

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("LaneWidth");
        float newWidth = EditorGUILayout.Slider(generator.roadWidth, 1, 10, null);
        if(newWidth != generator.roadWidth) {
            generator.roadWidth = newWidth;
            regenerateLanes();
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Radius");
        float newRadius = EditorGUILayout.Slider(generator.roadRadius, 1f, 100f, null);
        if(newRadius != generator.roadRadius) {
            generator.roadRadius = newRadius;
            regenerateLanes();
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Segments");
            int newSegments = EditorGUILayout.IntSlider(generator.segments, 10, 45, null);
            if(newSegments != generator.segments) {
            generator.segments = newSegments;
            regenerateLanes();
        }

        EditorGUILayout.EndHorizontal();



        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Lane Material");
        Material interiorMat = (Material) EditorGUILayout.ObjectField(generator.interiorMaterial,typeof(Material),true );
        if (interiorMat != generator.interiorMaterial) {
            generator.interiorMaterial = interiorMat;
            regenerateLanes();
        }

        EditorGUILayout.EndHorizontal();





    }


    private void regenerateLanes() {

        //Transform[] childs = path.transform.r
        while (generator.transform.childCount != 0) {
            DestroyImmediate(generator.transform.GetChild(0).gameObject);
        }

        for (int i = 0; i < generator.numLanes; i++) {

            Lane lane = Instantiate(generator.lanePrefab, generator.transform).gameObject.GetComponent<Lane>();

            lane.segments = generator.segments;
            lane.roadWidth = generator.roadWidth;
            lane.roadMaterial = generator.interiorMaterial;

            if (i == 0) {
                lane.roadRadius = generator.roadRadius;

            } else {
                lane.roadRadius = generator.roadRadius + 2 * generator.roadWidth * i;

            }


            //g.name = "Lane" + i.ToString();
            // g.transform.parent = generator.transform;
        }
    }

    private void OnSceneGUI() {

        generator = target as RoundaboutGenerator;
      
        //path.Start();
        Handles.color = Color.green;

       

        //drawLanesPath();
       

       /* for (int i = 0; i < path.pathPoints.Length-1; i++) {
            Handles.DrawLine(transform.TransformPoint(path.pathPoints[i]), transform.TransformPoint(path.pathPoints[i + 1]));
            //Debug.Log(i.ToString());
        }
        Handles.DrawLine(transform.TransformPoint(path.pathPoints[path.pathPoints.Length-1]), transform.TransformPoint(path.pathPoints[0]));*/

       /* Graphics.DrawMesh(meshFilter.sharedMesh,Matrix4x4.identity, renderer.material,LayerMask.NameToLayer("Default"));
        Repaint();*/
       
        //int index = path.getCutPointIndex();
        //Handles.color = Color.blue;
        

        /*if (Handles.Button(transform.TransformPoint(path.pathPoints[index]),Quaternion.identity, 2, 2, Handles.DotHandleCap)) {
            Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
        }
        */

        /*
        Handles.color = Color.red;
        if (index == 0) {
            if (Handles.Button(transform.TransformPoint(path.pathPoints[1]), Quaternion.identity, 2, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }

            if (Handles.Button(transform.TransformPoint(path.pathPoints[path.pathPoints.Length-1]), Quaternion.identity, 2, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }
        } else {

            if (Handles.Button(transform.TransformPoint(path.pathPoints[index+1]), Quaternion.identity, 4, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }

            if (Handles.Button(transform.TransformPoint(path.pathPoints[index - 1]), Quaternion.identity, 4, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }

        }

        */

    }

    private void drawLanesPath() {
       /* Handles.color = Color.blue;

        for(int i = 0; i < path.numLanes; i++) {
            Handles.DrawWireDisc(path.transform.position, Vector3.up, path.roadRadius + 3* path.roadWidth*i);
        }*/
    }
}
