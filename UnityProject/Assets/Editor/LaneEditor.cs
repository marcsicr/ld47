﻿
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Lane))]
public class LaneEditor : Editor
{
    private Lane path;

    public override void OnInspectorGUI() {

      
        path = target as Lane;

        int newSegments = path.segments;

        DrawDefaultInspector();
      

        EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Radius");
            path.roadRadius = EditorGUILayout.Slider(path.roadRadius, 1f, 100f, null);
            GUILayout.Label("Segments");
            path.segments = EditorGUILayout.IntSlider(path.segments, 10, 45, null);
            path.generateMesh();
        EditorGUILayout.EndHorizontal();


        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Lane Material");
        path.GetComponent<Renderer>().material = (Material)EditorGUILayout.ObjectField(path.roadMaterial, typeof(Material), true);
        EditorGUILayout.EndHorizontal();
        
       

       
    }

    private void OnSceneGUI() {

        path = target as Lane;
      
        //path.Start();
        Handles.color = Color.green;

        drawLanePath();
       

       /* for (int i = 0; i < path.pathPoints.Length-1; i++) {
            Handles.DrawLine(transform.TransformPoint(path.pathPoints[i]), transform.TransformPoint(path.pathPoints[i + 1]));
            //Debug.Log(i.ToString());
        }
        Handles.DrawLine(transform.TransformPoint(path.pathPoints[path.pathPoints.Length-1]), transform.TransformPoint(path.pathPoints[0]));*/

       /* Graphics.DrawMesh(meshFilter.sharedMesh,Matrix4x4.identity, renderer.material,LayerMask.NameToLayer("Default"));
        Repaint();*/
       
        //int index = path.getCutPointIndex();
        //Handles.color = Color.blue;
        

        /*if (Handles.Button(transform.TransformPoint(path.pathPoints[index]),Quaternion.identity, 2, 2, Handles.DotHandleCap)) {
            Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
        }
        */

        /*
        Handles.color = Color.red;
        if (index == 0) {
            if (Handles.Button(transform.TransformPoint(path.pathPoints[1]), Quaternion.identity, 2, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }

            if (Handles.Button(transform.TransformPoint(path.pathPoints[path.pathPoints.Length-1]), Quaternion.identity, 2, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }
        } else {

            if (Handles.Button(transform.TransformPoint(path.pathPoints[index+1]), Quaternion.identity, 4, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }

            if (Handles.Button(transform.TransformPoint(path.pathPoints[index - 1]), Quaternion.identity, 4, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }

        }

        */

    }

    private void drawLanePath() {
        Handles.color = Color.blue;
        Handles.DrawWireDisc(path.transform.position, Vector3.up, path.roadRadius);
        
    }
}
