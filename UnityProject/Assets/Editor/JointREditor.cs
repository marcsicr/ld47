﻿
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(JointR))]
public class JointREditor : Editor{

    private JointR jointR;

    public override void OnInspectorGUI() {
       

        jointR = target as JointR;

        HorizontalFloatSlider("Radius", ref jointR.radius, 1, 50);
        HorizontalIntSlider("Quarter Verts", ref jointR.quarterVerts, 0, 10);
        HorizontalFloatSlider("Start Angle", ref jointR.startAngle, 0, 80);
        HorizontalFloatSlider("Height", ref jointR.height, jointR.radius, jointR.radius + 50);
        HorizontalFloatSlider("Lines Height", ref jointR.heightLines, 0.5f, 1f);

        
    }


    private void OnSceneGUI() {

        jointR = target as JointR;

        if (jointR.transform.hasChanged) {
            onDataChanged();
        }

    }

    public void HorizontalFloatSlider(string label, ref float floatValue, float min, float max) {

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label(label);
        float newValue = EditorGUILayout.Slider(floatValue, min, max, null);
        if (newValue != floatValue) {
            floatValue = newValue;
            onDataChanged();
            SceneView.RepaintAll();
        }
        EditorGUILayout.EndHorizontal();
    }


    public void HorizontalIntSlider(string label, ref int intValue, int min, int max ) {

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label(label);
        int newValue = EditorGUILayout.IntSlider(intValue, min, max, null);
        if (newValue != intValue) {
            intValue = newValue;
            onDataChanged();
            SceneView.RepaintAll();
        }
        EditorGUILayout.EndHorizontal();
    }

    public void onDataChanged() {
        jointR.onDataChanged();
    }


}
