﻿
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Roundabout))]
public class RoadEditor : Editor
{
    private Roundabout path;

    public override void OnInspectorGUI() {

      
        path = target as Roundabout;

        int newSegments = path.segments;

        DrawDefaultInspector();
        if (GUILayout.Button("Reset")) {
            //mesh.Reset(); //2
            MeshFilter meshFilter = path.GetComponent<MeshFilter>();

        }

        EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Radius");
            path.roadRadius = EditorGUILayout.Slider(path.roadRadius, 1f, 100f, null);
            GUILayout.Label("Segments");
            path.segments = EditorGUILayout.IntSlider(path.segments, 10, 45, null);
            path.generateMesh();
        EditorGUILayout.EndHorizontal();

        
        EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Lanes");
            int newLanes = EditorGUILayout.IntSlider(path.numLanes, 1, 5, null);
        EditorGUILayout.EndHorizontal();

        if (newLanes != path.numLanes) {
            path.numLanes = newLanes;

            //Transform[] childs = path.transform.r
            while (path.transform.childCount != 0) {
                DestroyImmediate(path.transform.GetChild(0).gameObject);
            }

            for (int i = 0; i < path.numLanes; i++) {
                GameObject g = new GameObject();
                g.name = "Lane" + i.ToString();
                g.transform.parent = path.transform;
            }
        } 

      

        

        EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Lane Material");
        path.GetComponent<Renderer>().material = (Material)EditorGUILayout.ObjectField(path.roadMaterial, typeof(Material), true);
        EditorGUILayout.EndHorizontal();

       

       
    }

    private void OnSceneGUI() {

        path = target as Roundabout;
      
        //path.Start();
        Handles.color = Color.green;

       

        drawLanesPath();
       

       /* for (int i = 0; i < path.pathPoints.Length-1; i++) {
            Handles.DrawLine(transform.TransformPoint(path.pathPoints[i]), transform.TransformPoint(path.pathPoints[i + 1]));
            //Debug.Log(i.ToString());
        }
        Handles.DrawLine(transform.TransformPoint(path.pathPoints[path.pathPoints.Length-1]), transform.TransformPoint(path.pathPoints[0]));*/

       /* Graphics.DrawMesh(meshFilter.sharedMesh,Matrix4x4.identity, renderer.material,LayerMask.NameToLayer("Default"));
        Repaint();*/
       
        //int index = path.getCutPointIndex();
        //Handles.color = Color.blue;
        

        /*if (Handles.Button(transform.TransformPoint(path.pathPoints[index]),Quaternion.identity, 2, 2, Handles.DotHandleCap)) {
            Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
        }
        */

        /*
        Handles.color = Color.red;
        if (index == 0) {
            if (Handles.Button(transform.TransformPoint(path.pathPoints[1]), Quaternion.identity, 2, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }

            if (Handles.Button(transform.TransformPoint(path.pathPoints[path.pathPoints.Length-1]), Quaternion.identity, 2, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }
        } else {

            if (Handles.Button(transform.TransformPoint(path.pathPoints[index+1]), Quaternion.identity, 4, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }

            if (Handles.Button(transform.TransformPoint(path.pathPoints[index - 1]), Quaternion.identity, 4, 2, Handles.DotHandleCap)) {
                Repaint(); // Inspector dosen't refresh when we select a point in the scene view.
            }

        }

        */

    }

    private void drawLanesPath() {
        Handles.color = Color.blue;

        for(int i = 0; i < path.numLanes; i++) {
            Handles.DrawWireDisc(path.transform.position, Vector3.up, path.roadRadius + 3* path.roadWidth*i);
        }
    }
}
