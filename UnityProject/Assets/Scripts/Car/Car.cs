﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Car : MonoBehaviour
{

    public Vector3 centerOfMass;

    public List<Axle> axleInfos; // the information about each individual axle
    public float maxMotorTorque; // maximum torque the motor can apply to wheel
    public float maxSteeringAngle; // maximum steer angle the wheel can have



    public float decelerationForce;
    public float breakTorque;
    bool isBreaking = false;

    private float accelerator;
    private float steering;

    public float torqueScale = 1f;

    Rigidbody rb;

    public void Start() {

        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = centerOfMass;
    }



    public void Update() {

         accelerator = maxMotorTorque * Input.GetAxis("Vertical");
         steering = maxSteeringAngle * Input.GetAxis("Horizontal");
         isBreaking = Input.GetKey(KeyCode.Space);

      /*  Debug.ClearDeveloperConsole();
        Debug.Log("Velocity:" + Mathf.Abs(rb.velocity.magnitude));*/

    }

    public void FixedUpdate() {


        foreach (Axle axleInfo in axleInfos) {


            if (axleInfo.steering) {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }


            if (accelerator != 0 && !isBreaking) {
                axleInfo.leftWheel.brakeTorque = 0;
                axleInfo.rightWheel.brakeTorque = 0;

                if (axleInfo.motor) {
                    axleInfo.leftWheel.motorTorque = accelerator * torqueScale;
                    axleInfo.rightWheel.motorTorque = accelerator * torqueScale;
                }
            }

            if (isBreaking) {
                axleInfo.applyBreakForce(breakTorque * torqueScale);
            } 

          /*  if(accelerator == 0) {
                axleInfo.applyBreakForce(decelerationForce * torqueScale);
            }*/
            
     
        }

    }


    [System.Serializable]
    public class Axle {

        public Transform GFX_LEFT;
        public Transform GFX_RIGHT;

        public WheelCollider leftWheel;
        public WheelCollider rightWheel;
        public bool motor; // is this wheel attached to motor?
        public bool steering; // does this wheel apply steer angle?


        public void applyBreakForce(float breakeForce) {

            leftWheel.brakeTorque = breakeForce;
            rightWheel.brakeTorque = breakeForce;

            rightWheel.motorTorque = 0;
            leftWheel.motorTorque = 0;
        }
    }


}
