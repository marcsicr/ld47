﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour
{
    public WheelCollider wheel;

    private Vector3 position = new Vector3();
    private Quaternion rotation = new Quaternion();

    // Start is called before the first frame update
    void Start()
    {
        if (wheel == null)
            Debug.LogError("Missing wheel collider reference");
    }

    // Update is called once per frame
    void Update(){

        wheel.GetWorldPose(out position, out rotation);
        transform.position = position;
        transform.rotation = rotation;

        
    }
}
