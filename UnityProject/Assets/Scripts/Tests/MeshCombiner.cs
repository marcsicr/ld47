﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshCombiner : MonoBehaviour
{
    
    public MeshFilter first;
    public MeshFilter second;

    public Material fistMaterial;
    public Material secondMaterial;

    void Start() {
        if (first == null || second == null) Debug.LogError("MeshFilter is null");

        CombineInstance[] combines = new CombineInstance[2];

        combines[0].mesh = first.sharedMesh;
        combines[0].transform = first.transform.localToWorldMatrix;

        combines[1].mesh = second.sharedMesh;
        combines[1].transform = second.transform.localToWorldMatrix;


        MeshFilter meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = new Mesh();
        meshFilter.mesh.CombineMeshes(combines,false);

        MeshRenderer renderer = GetComponent<MeshRenderer>();
        renderer.materials = new Material[2];
        renderer.materials[0] = fistMaterial;
        renderer.materials[1] = secondMaterial;
       

    }

    // Update is called once per frame
    void Update()
    {
        
    }

   
}
