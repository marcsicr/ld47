﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
[ExecuteInEditMode]
public class Roundabout : MonoBehaviour {

    [HideInInspector]
    public Vector3[] pathPoints;

    public List<Vector3[]> lanePathPoints;

   // [Range(1f, 100f)]
    public float roadRadius = 2f;
    [Range(1f, 15f)]
    public float roadWidth = 1f;

    [HideInInspector]
    public int numLanes = 1;

    //[Range(10, 50)]
    public int segments = 10;
    public bool isClosed = true;

    public Material roadMaterial;

    public void Awake() {
        lanePathPoints = new List<Vector3[]>();
    }
    public void Start() {
        
        generateMesh();
        GetComponent<MeshRenderer>().material = roadMaterial;
    }

    Mesh CreateRoadMesh(Vector3[] points) {

        Vector3[] verts = new Vector3[points.Length * 2];

        int numTris = 2*(points.Length -1) + 2;
        int[] tris = new int[numTris * 3];
        Vector2[] uvs = new Vector2[verts.Length];
        int vertIndex = 0;
        int triIndex = 0;

      
        for(int i = 0; i < points.Length; i++) {
            Vector3 forward = Vector3.zero;

            if(i < points.Length - 1 || isClosed) {
                forward +=points[(i + 1)% points.Length ] - points[i];
            }

            if(i > 0 || isClosed) {
                forward += points[i] - points[(i - 1+points.Length)%points.Length];
                forward.Normalize();
                Vector3 left = new Vector3(-forward.y, forward.x,0);

                verts[vertIndex] = points[i] + left * roadWidth;
                verts[vertIndex + 1] = points[i] - left * roadWidth;

                float completionPercent;
                if (!isClosed)
                    completionPercent = i / (float)(points.Length - 1);
                else {
                    completionPercent = i / (float)(points.Length);
                }

                float v = 1 - Mathf.Abs(2 * completionPercent - 1);
                uvs[vertIndex] = new Vector2(0, v);
                uvs[vertIndex+1] = new Vector2(1,v);

                if ( i < points.Length -1 || isClosed) {
   
                        tris[triIndex] = vertIndex;
                        tris[triIndex + 1] = (vertIndex + 2) % verts.Length;
                        tris[triIndex + 2] = vertIndex + 1;

                        tris[triIndex + 3] = vertIndex + 1;
                        tris[triIndex + 4] = (vertIndex + 2) % verts.Length;
                        tris[triIndex + 5] = (vertIndex + 3) % verts.Length;
                    
                }
                vertIndex += 2;
                triIndex += 6;
            }
        }


        //Close Circle

        Mesh mesh = new Mesh();
        
        mesh.vertices = verts;
        mesh.triangles = tris;

        mesh.uv = uvs;
        return mesh;

        
    }

 

    public void generateMesh() {



        pathPoints = generateBaseCirclePositions();

       /* for(int i = 0; i < numLanes; i++) {
            lanePathPoints.Add(generateBaseCirclePositions(i));
        }*/
        
        
        //lanePathPoints.Add(pathPoints);
      


        // Vector3[] cuts = computeCuts(cuts);
        Mesh m = CreateRoadMesh(pathPoints);
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = m;
        GetComponent<MeshCollider>().sharedMesh = m;
    }

    private Vector3[] generateBaseCirclePositions(int laneIndex = 0) {

        float radius = roadRadius;// + roadWidth * laneIndex;
     
        float deltaTheta = (float)(2.0 * Mathf.PI) / segments; // Angle between each point
        Vector3[] points = new Vector3[segments];
        float theta = 0f;
        for (int i = 0; i < segments; i++) {
            //float x = roadRadius * Mathf.Cos(theta);
            //float y = roadRadius * Mathf.Sin(theta);
            float x = radius * Mathf.Cos(theta);
            float y = radius * Mathf.Sin(theta);

            points[i] = new Vector3(x, y, transform.position.z);
            theta += deltaTheta;
        }
        return points;
    }

}
