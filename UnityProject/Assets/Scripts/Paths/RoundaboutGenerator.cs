﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[ExecuteInEditMode]
public class RoundaboutGenerator : MonoBehaviour {

    public GameObject lanePrefab;
   
   [HideInInspector]
    public float roadRadius = 2f;

    [HideInInspector]
    public float roadWidth = 1f;

    [HideInInspector]
    public int numLanes = 1;

    [HideInInspector]
    public int segments = 10;
    [HideInInspector]
    public bool isClosed = true;

    public Material exteriorMaterial;
    public Material interiorMaterial;

    public void Awake() {
    }

    public void Start() {
        
        //Generate Lanes
    }


    
 
}
