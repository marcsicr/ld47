﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class JointR : MonoBehaviour
{
    public float radius = 5f;
    public int quarterVerts = 10;

    public float height;

    

    MeshRenderer meshRenderer;
    MeshFilter meshFilter;

    [HideInInspector]
    public float startAngle;

    [HideInInspector]
    public Vector3[] verts;

    [HideInInspector]
    public float heightLines;

    private Vector3 topLeftCorner { get { return verts[verts.Length - 2]; } }
    private Vector3 topRightCorner { get { return verts[verts.Length - 1]; } }

    void Start(){

        CreateMesh();

        Debug.Log("Started");

     
    }

    private void Update() {
        //CreateMesh();
    }

    public void onDataChanged() {

        CreateMesh();
    }

    void CreateMesh() {

        
        quarterPointsAroundCircleS(radius);
        
        int numTris = 3 + 2 * quarterVerts;

        int[] tris = new int[numTris *3];
        int trisIndex = 0;

        Vector2[] uvs = new Vector2[verts.Length];

     
        for (int i = 0; i <= quarterVerts; i++) {

            tris[trisIndex] = i;
            tris[trisIndex+1] = i+1;
            tris[trisIndex + 2] = verts.Length - 1; //Top right Corner
            trisIndex += 3;
        }

        

        tris[trisIndex] = quarterVerts + 1;
        tris[trisIndex + 1] = verts.Length-2;
        tris[trisIndex + 2] = verts.Length - 1;

       trisIndex += 3;
       
        for (int i = quarterVerts+1; i < verts.Length-3; i++) {

            tris[trisIndex] = i;
            tris[trisIndex + 1] = i + 1;
            tris[trisIndex + 2] = verts.Length - 2; //Top left Corner
            trisIndex += 3;
        }


        Mesh mesh = new Mesh();

        mesh.vertices = verts;
        mesh.triangles = tris;
        mesh.uv = uvs;
        mesh.RecalculateNormals();

        

        GetComponent<MeshFilter>().mesh = mesh;
      
    }

    Vector3[] quarterPointsAroundCircleS(float radius) {

        //float endAngle = 90f + (90f - startAngle);
      
        float stepAngle = (90f - startAngle) / (float)(quarterVerts+1);

        Vector3 center = transform.position;
        Vector3 baseVector = new Vector3(center.x + radius, center.y, center.z);

        verts = new Vector3[3 + 2 + 2 * quarterVerts];
        verts[0] = Quaternion.AngleAxis(startAngle, Vector3.forward) * baseVector;


        verts[quarterVerts+1] = Quaternion.AngleAxis(90f, Vector3.forward) * baseVector; //Center vertex


        verts[verts.Length-3] = Quaternion.AngleAxis(180f - startAngle, Vector3.forward) * baseVector;


        for (int i = 1; i <= quarterVerts; i++) {
            verts[i] = Quaternion.AngleAxis(startAngle + stepAngle * i, Vector3.forward) * baseVector;
            verts[i + quarterVerts + 1] = Quaternion.AngleAxis(90f + stepAngle * i, Vector3.forward) * baseVector;

        }

        verts[verts.Length-2] = new Vector3(verts[verts.Length -3].x, transform.position.y + height, transform.position.z); //left
        verts[verts.Length-1] = new Vector3(verts[0].x, transform.position.y + height, transform.position.z); //right


        return verts;
    }


        Vector3[] quarterPointsAroundCircle(float radius,Vector3 center) {

        // Minimum 3 points 1 each corner and 1 in the middle


        float angle = 90f / (float)(quarterVerts-1);
        Vector3[] verts = new Vector3[quarterVerts];

        Vector3 circleCenter = transform.position;
        Vector3 first = new Vector3(circleCenter.x+radius, circleCenter.y, circleCenter.z);
        
        for(int i = 0; i < quarterVerts-1; i++) {
            verts[i] =  Quaternion.AngleAxis(angle * i,Vector3.forward) * first;

            Debug.Log(angle * i);
        }

        verts[quarterVerts - 1] = Quaternion.AngleAxis(90f, Vector3.forward) * first;

        return verts;

    }

    private void OnDrawGizmosSelected() {
        
        
        
       //Debug.Log("OnDrawGizmosSelected");

        Gizmos.color = Color.red;
        foreach(Vector3 point in verts) {
            Gizmos.DrawSphere(transform.rotation *point, 0.25f);
        }

        Vector3 center = transform.position;

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere( transform.rotation * topLeftCorner,0.25f);
        
        Gizmos.DrawSphere(transform.rotation * topRightCorner, 0.25f);



    }

    

}
