﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class BezierPath : MonoBehaviour
{
    [SerializeField]
    private BezierSpline curvePath;
    [Range(1f, 100f)]
    public float roadRadius = 2f;
    [Range(1f, 25f)]
    public float roadWidth = 1f;

    
    public void Start() {

       // curvePath = gameObject.AddComponent<BezierSpline>();
    }
}
