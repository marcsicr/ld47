﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class FollowCarCamera : MonoBehaviour
{
    
    public Car car;
    public float mouseRotateXSpeed = 0.5f;
    public float mouseRotateYSpeed = 1f;
    Vector3 _cameraOffset;

    [Range(3,10)]
    public float maxHeight = 3f;

    [Range(1,2)]
    public float minHeight = 1f;

    Vector3 _cameraHeight;

    


    public void Start() {


        Vector3 backPosition = car.transform.TransformPoint(car.transform.localPosition  + Vector3.back * 0.25f + Vector3.up *1f);
        transform.position = backPosition;

        _cameraOffset = transform.position - car.transform.position;
       // _cameraHeight = new Vector3(0,minHeight,0);

        //direction = Vector3.left;


       // Vector3 carPosition = car.transform.position;

       // carPosition.x += direction * distance;

        //transform.position = car.transform.position + direction * distance;*/
    }

    private void LateUpdate() {

        transform.position = car.transform.position + Vector3.back * 30f + Vector3.up * 10f;
        transform.LookAt(car.transform);
        /*Quaternion camTurnAngleX = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * mouseRotateXSpeed, Vector3.up);

        Quaternion camTurnAngleY = Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * mouseRotateYSpeed, Vector3.right);

       // _cameraHeight.y = Mathf.Clamp(_cameraHeight.y + Input.GetAxis("Mouse Y") * mouseHeightSpeed, minHeight,maxHeight);

        _cameraOffset = camTurnAngleX * camTurnAngleY * _cameraOffset;

        //transform.position = car.transform.position + direction * distance;
        //transform.rotation = car.transform.rotation;


        transform.position = car.transform.position + _cameraOffset;// + _cameraHeight;
       
        transform.LookAt(car.transform);*/


    }
}
